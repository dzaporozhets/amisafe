# Am I safe?

Bash script to check if most obvious security features are enabled on my computer. 

## How to use - macOs

Copy command below, open Terminal app and paste it there. Press enter to execute and see output.

```
bash <(curl -s https://gitlab.com/dzaporozhets/amisafe/raw/master/macos)
```

The output should look like this:

```
FileVault is On.
Firewall On.
Flash Player not found.
```
